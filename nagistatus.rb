require 'sinatra'
require 'sinatra/reloader' if development?
require 'sinatra/config_file'
require 'json'

config_file 'settings.yml'

helpers do
  def get_status
    status_file = settings.status_file
    unless File.exists?(status_file)
      return {
        :success => false,
        :error   => 'Could not find status file'
      }
    end

    lines = File.readlines(status_file).map(&:chomp).reject do |line|
      line.empty? or line.match(/^\s*#/)
    end

    things = []
    thing = {}
    lines.each do |line|
      m = line.match(/^([a-z]+) *\{/)
      if m and m.length > 1
        thing = { :type => m[1] }
        thing[:details] = {}
        next
      end

      m = line.match(/^\s*([A-Za-z_]+)=(.+)/)
      if m and m.length > 2
        k, v = m[1..2]
        thing[:details][k.to_sym] = v
        next
      end

      m = line.match(/}$/)
      things << thing
    end

    status = things.inject({ :hosts => {} }) do |c, t|
      details = t[:details]
      type = t[:type]

      case type
      when /info/
        c[:info] = details
      when /servicestatus/
        hostname = details[:host_name]
        nid = details[:current_notification_id]
        unless c[:hosts].include?(hostname)
          c[:hosts][hostname] = {}
          c[:hosts][hostname][:checks] = {}
        end

        c[:hosts][hostname][:checks][nid] = details
      when /hoststatus/
        hostname = details[:host_name]
        unless c.include?(:host_statuses)
          c[:host_statuses] = {}
        end

        c[:host_statuses][hostname] = details
      end

      c
    end

    {
      :success  => true,
      :status => status
    }
  end

  def get_checks_for(status, hostname)
    if status[:success] and status[:status][:hosts].include?(hostname)
      status[:status][:hosts][hostname]
    else
      nil
    end
  end

  def get_status_for(status, hostname)
    if status[:success] and status[:status][:host_statuses].include?(hostname)
      status[:status][:host_statuses][hostname]
    else
      nil
    end
  end
end

get '/' do
  content_type :json
  get_status.to_json
end

get '/check/:hostname' do |hostname|
  content_type :json
  status = get_status
  checks = get_checks_for status, hostname
  if checks.nil?
    result = {
      :success => false,
      :error   => "Could not get checks for #{hostname}"
    }
  else
    result = {
      :success => true,
      :checks  => checks
    }
  end

  result.to_json
end

get '/status/:hostname' do |hostname|
  content_type :json
  status = get_status
  host_status = get_status_for status, hostname
  if host_status.nil?
    result = {
      :success => false,
      :error   => "Could not get host status for #{hostname}"
    }
  else
    result = {
      :success => true,
      :status  => host_status
    }
  end

  result.to_json
end

get '/down' do
  status = get_status
  if status[:success]
    down = status[:status][:host_statuses].keys.select do |hostname|
      status[:status][:host_statuses][hostname][:current_state].to_i == 1
    end

    result = {
      :success    => true,
      :down_hosts => down
    }
  else
    result = {
      :success => false,
      :error   => status[:error]
    }
  end

  result.to_json
end
