require 'sinatra'

require File.expand_path '../nagistatus.rb', __FILE__

map ('/nagistatus') { run Sinatra::Application }
